-- Find all artists that has letter d in its name.

SELECT * FROM artists WHERE name like "%d%";

-- Find all songs that has a length of less than 230.

SELECT * FROM songs WHERE length < 230;

-- Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)

SELECT albums.album_title, songs.song_name, songs.length
FROM albums
JOIN songs ON albums.id = songs.album_id;

-- Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)

SELECT * FROM albums
JOIN artists ON artists.id = albums.artist_id
WHERE album_title like "%a%";

-- Sort the albums in Z-A order. (Show only the first 4 records.)

SELECT * FROM albums ORDER BY album_title desc limit 4;

-- Join the 'albums' and 'songs' tables. (Sort albums from Z-A)

SELECT * FROM albums
JOIN songs ON songs.album_id = albums.id
ORDER BY album_title desc;